#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include "pwmfreq.h"

constexpr auto FadeTime = 2000;
constexpr auto PwmPin = 9;
constexpr auto PowerPin = 5;
constexpr unsigned long OnTime = 1ul * 60 * 1000;

volatile bool triggered = false;

void trigger()
{
    triggered = true;
}

void fade(uint8_t pin, unsigned int ms, int from, int to)
{
    const unsigned int range = fabs(to - from);
    const unsigned long t_delta = round(ms * 1000.0f / float(range));

    if (from < to) {
        for (int i = from; i <= to; ++i) {
            analogWrite(pin, i);
            delayMicroseconds(t_delta);
        }
    } else {
        for (int i = from; i >= to; --i) {
            analogWrite(pin, i);
            delayMicroseconds(t_delta);
        }
    }
}

void setup()
{
  Serial.begin(115200);
  SPCR = 0;  // disable SPI
  ADCSRA = 0;  // disable ADC
  power_adc_disable();
  power_twi_disable();
  power_spi_disable();

//  // Wake up on interrupt from 433 MHz receiver
  pinMode(2, INPUT);
  sleep_enable();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  attachInterrupt(0, trigger, HIGH);

  pinMode(PowerPin, OUTPUT);
  digitalWrite(PowerPin, HIGH);

  pinMode(PwmPin, OUTPUT);
  setPwmFrequency(9, 1);  // Set divisor on pin 9 to 1
  analogWrite(9, 0);
}

void loop()
{
    Serial.println("Going to sleep!");
    delay(100);
    sleep_cpu();

    Serial.println("Wakey, wakey!");
    fade(PwmPin, FadeTime, 0, 130);

    constexpr unsigned int DelayChunk = 1000;
    for (unsigned long slept = 0; slept < OnTime; slept += DelayChunk) {
        delay(DelayChunk);
        if (triggered) {
            Serial.println("Triggered during on!");
            triggered = false;
            slept = 0;
        }
    }

    fade(PwmPin, FadeTime, 130, 0);
    delay(100);
}
